#pragma once

#include <cstdint>

struct Event {
    uint64_t id = 0;
    uint32_t timestamp = 0;
};
