#pragma once

#include "data/Event.h"
#include "interfaces/IFunction.h"

#include <bits/stdc++.h>

class Filter {
public:
    Filter(std::unique_ptr<IFunction> f) : f_(std::move(f)) {}

    ~Filter() {
        stop();
    }

    void start() {
        thread_ = std::thread([this]() {
            while (shouldRun_) {
                auto val = getNext();
                if (!val) {
                    val = f_->process(val);
                }

                if (!val) {
                    std::this_thread::sleep_for(std::chrono::milliseconds(10));
                    continue;
                }

                filter(val);
            }
        });
    }

    void cancel() {
        shouldRun_ = false;
    }

    void stop() {
        cancel();
        if (thread_.joinable()) thread_.join();
    }

    void add(Event e) {
        std::scoped_lock lock(inputMutex_);
        input_.push_back(e);
    }
    //It should be possible to connect the output of a filter to the inputs of multiple other filters
    void connectOutputTo(std::shared_ptr<Filter> filter) {
        std::scoped_lock lock(nextFiltersMutex_);
        nextFilters_.push_back(filter);
    }

    //The output of a filter is the entire collection after the processing function has been executed.
    //This will only be filled when no other filters are connected - could be configured
    std::vector<Event> output() {
        std::scoped_lock lock(nextFiltersMutex_);
        std::vector<Event> ret;
        ret.swap(output_);
        return ret;
    }

private:

    std::optional<Event> getNext() {
        std::scoped_lock lock(inputMutex_);
        if (input_.empty()) return {};

        auto value = input_.back();
        input_.pop_back();
        return value;
    }

    void filter(std::optional<Event> e) {
        auto result = f_->process(e);

        if (!result) return;

        std::scoped_lock lock(nextFiltersMutex_);
        for (auto& nextFilter : nextFilters_) {
            nextFilter->add(result.value());
        }

        if (nextFilters_.empty()) {
            output_.push_back(result.value());
        }
    }

    std::unique_ptr<IFunction> f_;
    std::vector<std::shared_ptr<Filter>> nextFilters_;
    std::mutex nextFiltersMutex_;

    std::vector<Event> output_;

    std::vector<Event> input_;
    std::mutex inputMutex_;

    std::atomic_bool shouldRun_ = true;
    std::thread thread_;
};
