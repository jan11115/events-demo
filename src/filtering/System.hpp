#pragma once

#include "data/Event.h"
#include "Filter.hpp"

#include <bits/stdc++.h>

class FilteringSystem final {
public:
    ~FilteringSystem() {
        for (auto& [_, filters] : filters_) {
            for (auto& filter : filters) {
                filter->cancel();
            }
        }
        for (auto& [_, filters] : filters_) {
            for (auto& filter : filters) {
                filter->stop();
            }
        }
    }
    //filters added in rows, meaning row 1 connects to row 2 automatically, row 2 connects to row 3 automatically and so on
    //row 1 and 3 are not connected
    void add(size_t row, std::shared_ptr<Filter> f) {

        if (filters_.count(row + 1)) {
            auto& filters = filters_[row + 1];
            for (auto& filter : filters) {
                f->connectOutputTo(filter);
            }
        }

        if (row > 0 && filters_.count(row - 1)) {
            auto& filters = filters_[row - 1];
            for (auto& filter : filters) {
                filter->connectOutputTo(f);
            }
        }

        filters_[row].push_back(f);
    }

    std::vector<Event> getResult() {
        std::vector<Event> result;

        auto& filters = filters_.rbegin()->second;
        for (auto& filter : filters) {
            auto tempResult = filter->output();
            result.insert(result.cend(), tempResult.cbegin(), tempResult.cend());
        }

        return result;
    }

    void start() {
        for (auto& [_, filters] : filters_) {
            for (auto& filter : filters) {
                filter->start();
            }
        }
    }

private:
    std::map<size_t, std::vector<std::shared_ptr<Filter>>> filters_;
};
