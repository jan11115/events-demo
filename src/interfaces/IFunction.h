#pragma once

#include "data/Event.h"

#include <optional>

class IFunction {
public:
    virtual std::optional<Event> process(std::optional<Event> e) = 0;
    virtual ~IFunction() = default;
};
