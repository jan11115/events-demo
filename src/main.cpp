#include "data/Event.h"

//these 3 can be replaced with an include to a factory
#include "functions/Add.hpp"
#include "functions/Remove.hpp"
#include "functions/Transform.hpp"

#include "filtering/Filter.hpp"
#include "filtering/System.hpp"

#include <bits/stdc++.h>

void appendAddFilters(size_t row, FilteringSystem& system) {
    std::vector<Event> events1{ {0,1}, {1,2}, {6,8}, {20,100}, {30,1000} };
    auto add1 = std::make_shared<Filter>(std::make_unique<AddFunction>(events1));
    system.add(row, add1);


    std::vector<Event> events2{ {44,8}, {16,23}, {67,81}, {202,100}, {301,1000} };
    auto add2 = std::make_shared<Filter>(std::make_unique<AddFunction>(events2));
    system.add(row, add2);
}

void appendRemoveFilters(size_t row, FilteringSystem& system) {
    auto remove1 = std::make_shared<Filter>( std::make_unique<RemoveFunction>([](Event e) {
        return e.timestamp > 900;
    }) );
    system.add(row, remove1);

    auto remove2 = std::make_shared<Filter>(std::make_unique<RemoveFunction>([](Event e) {
        return e.timestamp > 20 && e.timestamp < 30;
    }) );
    system.add(row, remove2);
}

void appendTransformFilters(size_t row, FilteringSystem& system) {
    auto transform = std::make_shared<Filter>(std::make_unique<TransformFunction>([](Event e) {
        e.timestamp += 50;
        return e;
    }));
    system.add(row, transform);
}

int main() {
    size_t row = 0;
    FilteringSystem system;
    appendAddFilters(row++, system);
    appendRemoveFilters(row++, system);
    appendTransformFilters(row++, system);

    system.start();
    //10 events added in first row -> output is 10 events
    //each event passed to second row with 2 remove filters, each filter gets 10 events:
    //first filter removes 2 events 
    //second filter removes 1 event
    // output of second row is 17 events
    //last row only transforms events -> output is 17

    std::vector<Event> output;
    while (output.size() < 17) {
        auto result = system.getResult();
        output.insert(output.cend(), result.cbegin(), result.cend());
    }

    for (auto& event : output) {
	    std::cout << event.id << " " << event.timestamp << "\n";
    }

    return 0;
}
