#pragma once

#include "data/Event.h"
#include "interfaces/IFunction.h"

#include <vector>
#include <optional>
#include <functional>

class RemoveFunction : public IFunction {
public:
    RemoveFunction(std::function<bool(Event)> predicate) : predicate_(std::move(predicate)) {}

    std::optional<Event> process(std::optional<Event> e) override {
        if (!e) return {};
        if (predicate_(e.value())) return {};
        return e;
    }
private:
    std::function<bool(Event)> predicate_;
};
