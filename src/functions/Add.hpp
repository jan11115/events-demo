#pragma once

#include "data/Event.h"
#include "interfaces/IFunction.h"

#include <vector>
#include <optional>

class AddFunction : public IFunction {
public:
    AddFunction(std::vector<Event> events) : events_(std::move(events)) {}

    std::optional<Event> process(std::optional<Event> e) override {
        if (e) return e;
        if (events_.empty()) return {};

        auto ret = events_.back();
        events_.pop_back();
        return ret;
    }

private:
    std::vector<Event> events_;
};
