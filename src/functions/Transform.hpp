#pragma once

#include "data/Event.h"
#include "interfaces/IFunction.h"

#include <vector>
#include <optional>
#include <functional>

class TransformFunction : public IFunction {
public:
    TransformFunction(std::function<Event(Event)> predicate) : predicate_(std::move(predicate)) {}

    std::optional<Event> process(std::optional<Event> e) override {
        if (!e) return {};
        return predicate_(e.value());
    }
private:
    std::function<Event(Event)> predicate_;
};
